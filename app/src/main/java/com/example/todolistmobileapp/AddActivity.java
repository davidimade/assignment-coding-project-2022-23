package com.example.todolistmobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {

    //my edit texts and button from my activity_add xml file.


    EditText title_input, description_input;
    Button add_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);


        //identifying my information from xml files, add button, input and description

        title_input = findViewById(R.id.title_input);
        description_input = findViewById(R.id.description_input);
        add_button = findViewById(R.id.add_button);

        //when clicked, update the information and change it in my database

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyDBHelper myDB = new MyDBHelper(AddActivity.this);
                myDB.addTask(title_input.getText().toString().trim(),
                        description_input.getText().toString().trim());
            }
        });
    }

}