package com.example.todolistmobileapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    //has my list information such as task id, title and description which is shown on the task page.
   private Context context;
   Activity activity;
   private ArrayList task_id, task_title, task_description;


    CustomAdapter(Activity activity, Context context, ArrayList task_id, ArrayList task_title, ArrayList task_description){
        this.activity = activity;
        this.context = context;
        this.task_id = task_id;
        this.task_title = task_title;
        this.task_description = task_description;


    }

    //the view of how each row will look
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

//update the contents of the list to reflect the item at the given position
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.task_id_text.setText(String.valueOf(task_id
                .get(position)));
        holder.task_title_text.setText(String.valueOf(task_title
                .get(position)));
        holder.desc_title_text.setText(String.valueOf(task_description
                .get(position)));
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateActivity.class);
                intent.putExtra("id", String.valueOf(task_id.get(position)));
                intent.putExtra("title", String.valueOf(task_title.get(position)));
                intent.putExtra("description", String.valueOf(task_description.get(position)));
                activity.startActivityForResult(intent, 1);
            }
        });
        }

    @Override
    public int getItemCount() {
        return task_id.size();
    }




public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView task_id_text, task_title_text, desc_title_text;
    LinearLayout mainLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            task_id_text = itemView.findViewById(R.id.task_id_text);
            task_title_text = itemView.findViewById(R.id.task_title_text);
            desc_title_text = itemView.findViewById(R.id.desc_title_text);
            mainLayout = itemView.findViewById(R.id.mainLayout);

        }
    }
}
